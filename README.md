# Trees

# General Trees

A tree is an ADT that stores elements hierarchically. Each element in a tree, except the top element, has a parent element and zero o more children elements. The top element is called the root of the tree. 

We define a **tree** $T$ as a set of nodes storing elements such that the nodes have a **parent-child relationship** that satisfies the following properties

- If $T$ is nonempty, it has a special node, called the **root** of T, that has no parent.
- Each node $v$ of $T$ different from the root has a unique **parent** node $w$. We say that a node with parent $w$ is a **child** of $w$.

We can also define a tree recursively such that a tree $T$ is either empty or consists of a node $r$, called the root of $T$, and a set of trees whose roots are the children of $r$. 

A node $u$ is an **ancestor** of a node $v$ if $u = v$ or $u$ is an ancestor of the parent of $v$. Conversely, we say that a node $v$ is **descendant** of a node $u$ if $u$ is an ancestor of $v$. 

A **subtree** of $T$ rooted at a node $v$ is the tree consisting of all descendants of $v$ in $T$.  

An **edge** of tree $T$ is a pair of nodes $(u, v)$ such that $u$ is the parent of $v$, or vice versa. A **path** of $T$ is a sequence of nodes $u_1, \dots, u_n$ such that any two consecutives nodes in the sequence form an edge. 

We say that a tree is **ordered** if the children of each node are ordered. That is, for each node, we can identify whose the first child, the second one, and so on. 

Proposition. Let $T$ be a tree with $n$ positions, and let $c_p$ denote the number of children of a position $p$. Then $\sum_p c_p = n-1$. 

We define a tree ADT using the concept of position as abstraction for a node of a tree. A position object for a tree supports the method:

- `getElement():` Returns the element stored at this position.

The tree ADT supports the following accessor methods:

- `root():` Returns the position of the root of the three or null if empty.
- `parent(p):` Returns the position of the parent of position $p$ or null if $p$ is the root.
- `children(p):` Returns an iterable collection containing the children of $p$, if any.
- `numChildren(p):` Returns the number of children of $p$.

A tree ADT also supports the following query methods:

- `isInternal(p):` Returns true if $p$ has at least one child.
- `isExternal(p):` Returns true if $p$ does not have any children.
- `isRoot(p):` Return true if $p$ is the root of the three.

The tree ADT also support the next method which are not related with its structure:

- `size():` Returns the number of positions that are in the tree.
- `isEmpty():` Returns true if the tree is empty.
- `iterator():` Returns an iterator for all elements in the three (so that tree is Iterable).
- `positions():` Returns an iterable collection of all positions of the tree.

Let $p$ be a position within a tree $T$. The **depth** of $p$ is the number of ancestors of $p$ other that $p$ itself. We can also define the depth of $p$ recursively:

- If $p$ is the root, then the depth of $p$ is zero.
- Otherwise, depth of $p$ is the depth of the parent of $p$ plus one.

The recursive definition leads us to the following method:

```java
public int depth(Position<E> p) {
	if (isRoot(p))
		return 0;
	return 1+depth(parent(p));
}
```

The running time of depth(p) for a position $p$ is $O(d_p+1)$, where $d_p$ denotes the depth of $p$ in the tree. 

We define the **height** of a position $p$ in a tree recursively as follows:

- If $p$ is a leaf, the height of $p$ is zero.
- Otherwise, the height of $p$ is the maximum of heights of the children of $p$ plus one.

The recursive definition lead us to the following method: 

```java
public int height(Position<E> p) {
	int h = 0;
	for (Position<E> child : children(p))
		h = Math.max(h, 1+height(child));
	return h;
}
```

The **height** of a tree is the height of its root. 

Assuming children(p) is executed in $O(c_p + 1)$ time, where $c_p$ denotes the number of children of $p$, then the method spends $O(c_p+1)$ time at each position $p$ to compute the maximum. Therefore the time complexity to calculate the height of a tree with $n$ positions is: 

$O(\sum_p (c_p+1)) = O(n+\sum_p c_p) = O(n+n-1) = O(n).$ 

# Binary Tress

A binary tree is an ordered tree with the following properties:

1. Every node has at most two children.
2. Each child node can be identified as being either a **left child** or a **right child**. 
3. A left child precedes a right child in the order of children of a node. 

A subtree rooted at a left or right child of an internal node $v$ is called the **left subtree** or **right subtree**, respectively, of $v$.  

A binary tree is **proper** or **full** is each node has either zero or two children. 

We can also define a tree recursively. A binary tree is either

- An empty tree
- A nonempty tree having a root $r$, and two binary trees that are respectively the left and right subtrees of $r$.

The binary tree ADT is an specialization of a tree ADT, that supports three additional accessor methods:

- `left(p):` Returns the position of the left child of $p$ or null if $p$ has no left child.
- `right(p):` Returns the position of the right child of $p$ or null if $p$ has no right child.
- `sibling(p):` Return the position of the sibling of $p$ or null is $p$ has no sibling.

We define the **level** $d$ of a tree $T$ as the set of all nodes of $T$ at the same depth $d$. In particular, for a binary tree the level $d$ has at most $2^d$ nodes. 

Proposition. In a nonempty proper binary tree $T$, with $n_E$ external nodes and $n_I$ internal nodes, we have $n_E = n_I+1$. 

# Implementation

We can represent a binary tree using a linked structure, with a node that maintains references to the element stored at position $p$ and to the nodes associated with the left child, right child and parent of $p$. The three itself maintains an instance variable storing a reference to the root node (if any), and a variable called size that represents the number of nodes in the tree. 

For a linked binary tree, we present the following update methods: 

- `addRoot(e):` Creates a root for an empty tree, storing element $e$, and returns the position of that root.
- `addLeft(p, e):` Creates a left child of position $p$, storing element $e$, and return the position of the new node.
- `addRight(p, e):` Creates a right child of position $p$, storing element $e$, and return the position of the new node.
- `set(p, e):` Replaces the element stored at position $p$ with element $e$ and returns the previously stored element.
- `attach(p, T1, T2):` Attaches the internal structure of trees $T_1$ and $T_2$ as the respective left and right subtrees of the external position $p$.
- `remove(p):` Removes the node at position $p$, replacing it with its child (if any), and return the element that had been stored at $p$. An error occurs if $p$ has two children.

Each of these operations can be implemented in $O(1)$ with the linked representation. 

For a general tree we can use the linked structure to implement it. So, each node store a single container of references to its children. 

# Tree Traversal Algorithms

A **traversal** of a tree $T$ is a systematic way of “visiting” all the positions of $T$. 

For a general tree $T$ we have the following traversal: 

- **Preorder:** The root is visited first and then the subtrees rooted at its children are traversed recursively.
- **Postorder:** Traverse recursively the subtrees rooted at the children of the root, and then visit the root.

```latex
// Pseudocode
preorder(p) {
	visit p
	for each child c in children(p)
		postorder(c);
}
```

```latex
// Pseudocode
postorder(p) {
	for each child c in children(p)
		postorder(c);
	visit p
}
```

The time complexity of both algorithms is $O(n)$. The analysis is similar to that of algorithm `height` above. 

Another approach is the **breadth-first traversal**, in which we traverse a tree so that we visit all the positions at depth $d$ before we visit the positions at depth $d+1$. 

```latex
// Pseudocode
breadthFirst() {
	initialize queue Q to contain root()
	while Q is not empty 
		p = Q.dequeue()
		visit p
		for each child c in children(p)
			Q.enqueue(c)
}
```

In the case of a binary tree, we have the **inorder traversal**. For every position $p$, we visit $p$ after all positions in the left subtree of $p$ and before any position in the right subtree of $p$. 

```latex
// Pseudocode
inorder(p) {
	if p has a left child lc
		inorder(lc)
	visit p
	if p has a right child rc
		inorder(rc)
}
```
