package org.example;

public interface BinaryTree<E> extends Tree<E> {
    /**
     * Returns the Position of the left child
     * @param position
     * @return the left child Position or null if no left child exists
     * @throws IllegalArgumentException if the given position is not valid
     */
    Position<E> left(Position<E> position) throws IllegalArgumentException;

    /**
     * Returns the Position of the right child
     * @param position
     * @return the right child Position or null if no right child exists
     * @throws IllegalArgumentException if the given position is not valid
     */
    Position<E> right(Position<E> position) throws IllegalArgumentException;

    /**
     * Returns the Position of position's sibling
     * @param position
     * @return the sibling Position or null if no sibling exists
     * @throws IllegalArgumentException if the given position is not valid
     */
    Position<E> sibling(Position<E> position) throws IllegalArgumentException;
}
