package org.example;

import java.util.ArrayList;
import java.util.List;
public abstract class AbstractBinaryTree<E> extends AbstractTree<E>
                                    implements BinaryTree<E> {
    @Override
    public Position<E> sibling(Position<E> position) throws IllegalArgumentException {
        Position<E> parent = this.parent(position);
        if (parent == null)
            return null;
        if (position == this.left(parent))
            return this.right(parent);
        return this.left(parent);
    }

    @Override
    public int numChildren(Position<E> position) {
        int count = 0;
        if (this.left(position) != null)
            count++;
        if (this.right(position) != null)
            count++;
        return count;
    }

    public Iterable<Position<E>> children(Position<E> position) {
        List<Position<E>> snapshot = new ArrayList<>(2); // max capacity for a binary tree
        if (this.left(position) != null)
            snapshot.add(left(position));
        if (this.right(position) != null)
            snapshot.add(right(position));
        return snapshot;
    }

    /**
     * Adds positions of the subtree rooted at the position to the given snapshot
     * @param position root position of the subtree.
     * @param snapshot container for the positions.
     */
    private void inorderSubtree(Position<E> position, List<Position<E>> snapshot) {
        if (this.left(position) != null)
            inorderSubtree(this.left(position), snapshot);
        snapshot.add(position);
        if (this.right(position) != null)
            inorderSubtree(this.right(position), snapshot);
    }

    /**
     * Returns an iterable collection of positions of the three, reported in inorder.
     * @return an iterable collection of positions.
     */

    public Iterable<Position<E>> inorder() {
        List<Position<E>> snapshot = new ArrayList<>();
        if (!this.isEmpty())
            inorderSubtree(this.root(), snapshot);
        return snapshot;
    }

    /**
     * Overrides positions() method to make inorder the default order for binary trees.
     * @return an iterable collection of positions reported in inorder.
     */
    @Override
    public Iterable<Position<E>> positions() {
        return inorder();
    }
}
