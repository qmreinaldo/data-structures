package org.example;

import java.util.Iterator;

public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    protected static class Node<E> implements Position<E> {
        private E element;
        private Node<E> parent;
        private Node<E> left;
        private Node<E> right;
        public Node(E element, Node<E> parent, Node<E> left, Node<E> right) {
            this.element = element;
            this.parent = parent;
            this.left = left;
            this.right = right;
        }
        @Override
        public E getElement() throws IllegalStateException {
            return element;
        }
        public Node<E> getParent() {
            return this.parent;
        }
        public Node<E> getLeft() {
            return this.left;
        }
        public Node<E> getRight() {
            return this.right;
        }
        public void setElement(E element) {
            this.element = element;
        }
        public void setParent(Node<E> parent) {
            this.parent = parent;
        }
        public void setLeft(Node<E> left) {
            this.left = left;
        }
        public void setRight(Node<E> right) {
            this.right = right;
        }
    }
    /* Factory method to create a new node storing an element. */
    protected Node<E> createNode(E element, Node<E> parent, Node<E> left, Node<E> right) {
        return new Node<E>(element, parent, left, right);
    }

    protected Node<E> root = null;
    private int size = 0;
    public LinkedBinaryTree() {

    }
    /* Validates the position and returns is as a node. */
    protected Node<E> validate(Position<E> position) throws IllegalArgumentException {
        if (!(position instanceof Node))
            throw new IllegalArgumentException("Not valid position type");
        Node<E> node = (Node<E>) position;
        if (node.getParent() == node)           // convention for defunct node
            throw new IllegalArgumentException("position is no longer in the tree");
        return node;
    }
    public int size() {
        return this.size;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    public Position<E> root() {
        return this.root;
    }

    public Position<E> parent(Position<E> position) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        return node.getParent();
    }

    @Override
    public Position<E> left(Position<E> position) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        return node.getLeft();
    }


    @Override
    public Position<E> right(Position<E> position) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        return node.getRight();
    }

    public Position<E> addRoot(E element) throws IllegalStateException {
        if (!this.isEmpty())
            throw new IllegalStateException("Tree is not empty");
        this.root = createNode(element, null, null, null);
        this.size = 1;
        return this.root;
    }
    public Position<E> addLeft(Position<E> position, E element) throws IllegalArgumentException {
        Node<E> parent = this.validate(position);
        if (parent.getLeft() != null)
            throw new IllegalArgumentException("position already has a left child");
        Node<E> leftChild = createNode(element, parent, null, null);
        parent.setLeft(leftChild);
        return leftChild;
    }
    public Position<E> addRight(Position<E> position, E element) throws IllegalArgumentException {
        Node<E> parent = this.validate(position);
        if (parent.getRight() != null)
            throw new IllegalArgumentException("position already has a right child");
        Node<E> rightChild = createNode(element, parent, null, null);
        parent.setRight(rightChild);
        return rightChild;
    }

    public E set(Position<E> position, E element) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        E temp = node.getElement();
        node.setElement(element);
        return temp;
    }

    /**
     * Attaches trees leftTree and rightTree as left and right subtrees of the leaf position
     * @param position
     * @param leftTree
     * @param rightTree
     * @throws IllegalArgumentException if position is internal
     */
    public void attach(Position<E> position, LinkedBinaryTree<E> leftTree, LinkedBinaryTree<E> rightTree) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        if (this.isInternal(position))
            throw new IllegalArgumentException("position must be leaf");
        this.size += leftTree.size() + rightTree.size();
        if (!leftTree.isEmpty()) {
            leftTree.root.setParent(node);
            node.setLeft(leftTree.root);
            leftTree.root = null;
            leftTree.size = 0;
        }
        if (!rightTree.isEmpty()) {
            rightTree.root.setParent(node);
            node.setRight(rightTree.root);
            rightTree.root = null;
            rightTree.size = 0;
        }
    }

    /**
     * Removes the node at position and replaces it with its child, if any
     * @param position
     * @return the element stored at the removed position
     * @throws IllegalArgumentException if the position has two children
     */
    public E remove(Position<E> position) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        if (this.numChildren(position) == 2)
            throw new IllegalArgumentException("position has two children");
        Node<E> child = node.getLeft() != null ? node.getLeft() : node.getRight();
        if (child != null)
            child.setParent(node.getParent());      // child's grandparent becomes its parent
        if (node == this.root)
            this.root = child;                      // child becomes root
        else {
            Node<E> parent = node.getParent();
            if (node == parent.getLeft())
                parent.setLeft(child);
            else
                parent.setRight(child);
        }
        size--;
        E temp = node.getElement();
        node.setLeft(null);
        node.setRight(null);
        node.setElement(null);
        node.setParent(node);                       // convention for defunct node
        return temp;
    }
}
