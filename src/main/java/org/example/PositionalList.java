package org.example;

import java.util.Iterator;

public interface PositionalList<E> extends Iterable<E> {
    int size();

    boolean isEmpty();

    Position<E> first();

    Position<E> last();

    /**
     * Returns the Position immediately before the given position
     * @param position
     * @return the Position immediately before position or null if position is first
     * @throws IllegalStateException
     */
    Position<E> before(Position<E> position) throws IllegalStateException;

    /**
     * Returns the Position immediately after the given position
     * @param position
     * @return the Position immediately after position or null if position is last
     * @throws IllegalStateException
     */
    Position<E> after(Position<E> position) throws IllegalStateException;

    /**
     * Inserts the given element at the front of the list and returns its new Position
     * @param element
     * @return the new Position storing the given element
     */
    Position<E> addFirst(E element);

    /**
     * Inserts the given element at the back of the list and returns its new Position
     * @param element
     * @return the new Position storing the given element
     */
    Position<E> addLast(E element);

    /**
     * Inserts the given element immediately before the given position
     * @param position
     * @param element
     * @return the new Position storing the given element
     */
    Position<E> addBefore(Position<E> position, E element);

    /**
     * Inserts the given element immediately after the given position
     * @param position
     * @param element
     * @return the new Position storing the given element
     */
    Position<E> addAfter(Position<E> position, E element);

    E set(Position<E> position, E element) throws IllegalStateException;

    E remove(Position<E> position) throws IllegalStateException;

    Iterator<E> iterator();
}
