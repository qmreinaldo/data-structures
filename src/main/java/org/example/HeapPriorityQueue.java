package org.example;

import java.util.Comparator;

public class HeapPriorityQueue<K, V> extends AbstractPriorityQueue<K, V> {
    protected ArrayList<Entry<K, V>> heap = new ArrayList<>();
    public HeapPriorityQueue() {
        super();
    }
    public HeapPriorityQueue(Comparator<K> comparator) {
        super(comparator);
    }
    public HeapPriorityQueue(K[] keys, V[] values) {
        super();
        for (int i = 0; i < keys.length; i++)
            heap.add(i, new PQEntry<>(keys[i], values[i]));
        heapify();
    }
    private void heapify() {
        int startIndex = parentIndex(size()-1); // Start at the parent of the last node
        for (int i = startIndex; i >= 0; i--)   // iterate until we reach the root
            downHeap(i);
    }
    protected int parentIndex(int index) {
        return (index - 1) / 2;
    }
    protected int leftIndex(int index) {
        return 2 * index + 1;
    }
    protected int rightIndex(int index) {
        return 2 * index + 2;
    }

    protected boolean hasLeft(int index) {
        return this.leftIndex(index) < this.heap.size();
    }
    protected boolean hasRight(int index) {
        return this.rightIndex(index) < this.heap.size();
    }

    protected void swap(int indexA, int indexB) {
        Entry<K, V> aux = heap.get(indexA);
        heap.set(indexA, heap.get(indexB));
        heap.set(indexB, aux);
    }

    protected void upHeap(int index) {
        while (index > 0) {     // while we don't reach the root
            int parentIndex = parentIndex(index);
            if (compare(heap.get(index), heap.get(parentIndex)) >= 0) // correct position found
                break;
            swap(index, parentIndex);
            index = parentIndex;    // we continue upward
        }
    }
    protected void downHeap(int index) {
        while (hasLeft(index)) {        // while we don't reach a leaf
            int leftIndex = leftIndex(index);
            int smallChildIndex = leftIndex;
            if (hasRight(index)) {
                int rightIndex = rightIndex(index);
                if (compare(heap.get(leftIndex), heap.get(rightIndex)) > 0)
                    smallChildIndex = rightIndex;
            }
            if (compare(heap.get(smallChildIndex), heap.get(index)) >= 0)   // correct position found
                break;
            swap(index, smallChildIndex);
            index = smallChildIndex;    // we continue downward
        }
    }


    @Override
    public int size() {
        return heap.size();
    }

    @Override
    public Entry<K, V> insert(K key, V value) {
        Entry<K, V> newest = new PQEntry<>(key, value);
        heap.add(heap.size()-1, newest);    // add the new node at the end
        upHeap(heap.size()-1);  // place newest in the correct position
        return newest;

    }

    @Override
    public Entry<K, V> min() {
        if (heap.isEmpty())
            return null;
        return heap.get(0);
    }

    @Override
    public Entry<K, V> removeMin() {
        if (heap.isEmpty())
            return null;
        Entry<K, V> ans = heap.get(0);  // root
        swap(0, heap.size()-1); // swap the root and the last entry
        heap.remove(heap.size() -1);    // remove the last entry
        downHeap(0);    // place the node at root in the correct position
        return ans;
    }
}
