package org.example;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractTree<E> implements Tree<E> {

    private class ElementIterator implements Iterator<E> {
        Iterator<Position<E>> positionIterator = AbstractTree.this.positions().iterator();
        @Override
        public boolean hasNext() {
            return positionIterator.hasNext();
        }

        @Override
        public E next() {
            return positionIterator.next().getElement();
        }

        @Override
        public void remove() {
            positionIterator.remove();
        }
    }


    @Override
    public boolean isInternal(Position<E> position) throws IllegalArgumentException {
        return this.numChildren(position) > 0 ;
    }

    @Override
    public boolean isExternal(Position<E> position) throws IllegalArgumentException {
        return this.numChildren(position) == 0;
    }

    @Override
    public boolean isRoot(Position<E> position) throws IllegalArgumentException {
        return position == this.root();
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    public int depth(Position<E> position) {
        if (this.isRoot(position))
            return 0;
        return 1 + depth(this.parent(position));
    }
    public int height(Position<E> position) {
        int h = 0;                      //base case if position is a leaf
        for (Position<E> c : this.children(position))
            h = Math.max(h, 1+this.height(c));
        return h;
    }
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * Returns an iterable collection of positions of the tree using preorder algorithm as default
     * @return Returns an iterable collection of positions
     */
    public Iterable<Position<E>> positions() {
        return preorder();
    }

    /**
     * Adds the positions of the subtree rooted at position to the given snapshot using preorder traverse.
     * @param position
     * @param snapshot list that serves as a buffer to which visited positions are added
     */
    private void preorderSubtree(Position<E> position, List<Position<E>> snapshot) {
        snapshot.add(position);             // add position before exploring subtree
        for (Position<E> children : this.children(position))
            preorderSubtree(children, snapshot);
    }

    /**
     * Returns an iterable collection of positions of the tree, reported in preorder using preorderSubtree() method
     * @return an iterable collection of positions
     */
    public Iterable<Position<E>> preorder() {
        List<Position<E>> snapshot = new ArrayList<>();
        if (!this.isEmpty())
            this.preorderSubtree(this.root(), snapshot);    // fill the snapshot recursively
        return snapshot;
    }

    /**
     * Adds the positions of the subtree rooted at position to the given snapshot using postorder traverse.
     * @param position
     * @param snapshot list that serves as a buffer to which visited positions are added
     */
    private void postorderSubtree(Position<E> position, List<Position<E>> snapshot) {
        for(Position<E> children : this.children(position))
            postorderSubtree(children, snapshot);
        snapshot.add(position);
    }
    /**
     * Returns an iterable collection of positions of the tree, reported in postorder using postorderSubtree() method
     * @return an iterable collection of positions
     */
    public Iterable<Position<E>> postorder() {
        List<Position<E>> snapshot = new ArrayList<>();
        if (!this.isEmpty())
            this.postorderSubtree(this.root(), snapshot);
        return snapshot;
    }

    public Iterable<Position<E>> breadthFirst() {
        List<Position<E>> snapshot = new ArrayList<>();
        if (!this.isEmpty()) {
            Queue<Position<E>> queue = new LinkedQueue<>();
            queue.enqueue(this.root());
            while (!queue.isEmpty()) {
                Position<E> position = queue.dequeue();
                snapshot.add(position);
                for (Position<E> child : this.children(position))
                    queue.enqueue(child);
            }
        }
        return snapshot;
    }
}
