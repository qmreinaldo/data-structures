package org.example.applications;

import org.example.LinkedPositionalList;
import org.example.Position;
import org.example.PositionalList;

public class FavoritesListMoveToFront<E> extends FavoritesList<E>{

    /**
     * Moves itemPosition to the front of the list
     * @param itemPosition
     */
    @Override
    protected void moveUp(Position<Item<E>> itemPosition) {
        if (itemPosition != super.list.first())
            super.list.addFirst(super.list.remove(itemPosition));
    }

    @Override
    public Iterable getFavorites(int kFirst) throws IllegalStateException {
        if (kFirst < 0 || kFirst > super.list.size())
            throw new IllegalArgumentException("Invalid kFirst");

        // make a copy of the original list
        PositionalList<Item<E>> tempList = new LinkedPositionalList<>();
        for (Item<E> item: super.list)
            tempList.addLast(item);

        // repeated find, report, and remove item with the largest count
        PositionalList<E> result = new LinkedPositionalList<>();
        for (int i = 0; i < kFirst; i++) {
            Position<Item<E>> highPos = tempList.first();
            Position<Item<E>> walk = tempList.after(highPos);
            while (walk != null) {
                if (super.count(walk) > super.count(highPos))
                    highPos = walk;
                walk = tempList.after(walk);
            }
            // We have found item with the highest count (until now)
            result.addLast(super.value(highPos));
            tempList.remove(highPos);
        }
        return result;
    }
}
