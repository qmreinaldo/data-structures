package org.example.applications;

import org.example.LinkedPositionalList;
import org.example.Position;
import org.example.PositionalList;

import java.util.Iterator;

public class FavoritesList<E> {
    protected static class Item<E> {
        private E value;
        private int count = 0;
        public Item (E value) {
            this.value = value;
        }
        public int getCount() {
            return this.count;
        }
        public E getValue() {
            return this.value;
        }
        public void increment() {
            this.count++;
        }
    }
    PositionalList<Item<E>> list = new LinkedPositionalList<>();
    public FavoritesList() {

    }
    protected E value(Position<Item<E>> itemPosition) {
        return itemPosition.getElement().getValue();
    }
    protected int count(Position<Item<E>> itemPosition) {
        return itemPosition.getElement().getCount();
    }

    /**
     * Returns Position having element equal to element
     * @param element
     * @return Position having element attribute equal to element or null if not found
     */
    protected Position<Item<E>> findPosition(E element) {
        Position<Item<E>> walk = list.first();
        while (walk != null && element.equals(this.value(walk)))
            walk = list.after(walk);
        return walk;
    }

    /**
     * Moves item at Position itemPosition earlier in the list field based on access count
     * @param itemPosition
     */
    protected void moveUp(Position<Item<E>> itemPosition) {
        int accessCount = this.count(itemPosition);
        Position<Item<E>> walk = itemPosition;
        while (walk != this.list.first() && this.count(list.before(walk)) < accessCount) {
            walk = list.before(walk);
        }
        if (walk != itemPosition)
            list.addBefore(walk, list.remove(itemPosition));
    }
    public int size() {
        return this.list.size();
    }
    public boolean isEmpty() {
        return this.list.isEmpty();
    }
    public void access(E element) {
        Position<Item<E>> position = findPosition(element); // locate existing element (if any)_
        if (position == null)
            position = list.addLast(new Item<>(element));   // if new, place at end
        position.getElement().increment();                  // always increment count
        moveUp(position);                                   // always consider moving forward
    }
    public void remove(E element) {
        Position<Item<E>> position = this.findPosition(element); // locate existing element (if any)
        if (position != null)
            this.list.remove(position);
    }
    public Iterable<E> getFavorites(int kFirst) throws IllegalStateException {
        if (kFirst < 0 || kFirst > this.size())
            throw new IllegalArgumentException("Invalid kFirst");
        PositionalList<E> result = new LinkedPositionalList<>();
        Iterator<Item<E>> iterator = this.list.iterator();
        for (int i = 0; i < kFirst; i++) {
            result.addLast(iterator.next().getValue());
        }
        return result;
    }
}
