package org.example.applications;

import org.example.Position;
import org.example.PositionalList;

public class insertionSort {
    public static void insertionSort(PositionalList<Integer> list) {
        Position<Integer> marker = list.first();    // last position of the sorted subarray
        while (marker != list.last()) {
            Position<Integer> pivot = list.after(marker);
            int value = pivot.getElement();
            if (value > marker.getElement())
                marker = pivot;                     // pivot is already sorted
            else {
                Position<Integer> walk = marker;
                while (walk != list.first() && list.before(walk).getElement() > value) {
                    walk = list.before(walk);
                }
                list.remove(pivot);
                list.addBefore(walk, value);
            }
        }
    }
}
