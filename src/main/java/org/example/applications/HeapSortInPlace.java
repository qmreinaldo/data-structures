package org.example.applications;

import java.util.Arrays;


public class HeapSortInPlace {
    private final int[] array;
    public HeapSortInPlace(int[] array) {
        this.array = array;
    }
    public void heapSort() {
        int n = array.length;
        constructHeap();
        for (int i = n-1; i >= 1; i--) {
            swap(0, i);
            downHeap(0, i-1);
        }
    }

    private void swap(int from, int to) {
        int temp = array[to];
        array[to] = array[from];
        array[from] = temp;
    }

    public void constructHeap() {
        for (int i = parent(array.length-1); i >= 0; i--)
            downHeap(i, array.length-1);
    }

    private void downHeap(int i, int rightLimit) {
        while (hasLeft(i, rightLimit)) {
            int left = left(i);
            int maxChild = left;
            if (hasRight(i, rightLimit))
                if (array[right(i)] > array[left])
                    maxChild = right(i);
            if (array[i] >= array[maxChild])
                break;
            swap(i, maxChild);
            i = maxChild;
        }
    }

    private boolean hasRight(int i, int rightLimit) {
        return right(i) <= rightLimit;
    }

    private boolean hasLeft(int i, int rightLimit) {
        return left(i) <= rightLimit;
    }

    private int parent(int i) {
        return (i-1)/2;
    }

    private int left(int i) {
        return 2*i+1;
    }
    private int right(int i) {
        return 2*i+2;
    }

    public static void main(String[] args) {
        int[] array = new int[]{5, 2, 9, 7, 4, 6};
        HeapSortInPlace heapSort = new HeapSortInPlace(array);
        System.out.println(Arrays.toString(heapSort.array));
        heapSort.heapSort();
        System.out.println(Arrays.toString(heapSort.array));
    }
}
