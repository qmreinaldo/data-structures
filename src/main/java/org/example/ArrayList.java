package org.example;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E>{
    private class ArrayIterator implements Iterator<E> {
        private int indexOfNextElement = 0;
        private boolean removable = false;

        /**
         * Test whether the iterator has a next object
         * @return true if there are further objects, false otherwise
         */
        @Override
        public boolean hasNext() {
            return indexOfNextElement < ArrayList.this.size;
        }

        @Override
        public E next() throws NoSuchElementException {
            if (indexOfNextElement == ArrayList.this.size)
                throw new NoSuchElementException("No next element");
            this.removable = true;  // this element can subsequently be removed
            return ArrayList.this.data[indexOfNextElement++];
        }


        /**
         * Removes the element returned by the most recent call to next()
         * @throws IllegalStateException if next() has not yet been called
         * @throws IllegalStateException if remove was already called since recent next()
         */
        @Override
        public void remove() throws IllegalStateException{
            if (!this.removable)
                throw new IllegalStateException("nothing to remove");
            ArrayList.this.remove(indexOfNextElement-1);
            indexOfNextElement--;
            this.removable = false;     // do not allow remove again until next() is called
        }
    }
    public static final int CAPACITY = 16;

    private E[] data;
    private int size = 0;
    public ArrayList(int capacity) {
        data = (E[]) new Object[capacity];
    }
    public ArrayList() {
        this(CAPACITY);
    }
    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public E get(int index) throws IndexOutOfBoundsException {
        this.checkIndex(index, this.size);
        return this.data[index];
    }

    @Override
    public E set(int index, E element) throws IndexOutOfBoundsException {
        this.checkIndex(index, this.size);
        E temp = data[index];
        this.data[index] = element;
        return temp;
    }

    @Override
    public void add(int index, E element) throws IndexOutOfBoundsException {
        this.checkIndex(index, this.size+1);
        if (this.size == data.length)
            this.resize(2*this.data.length);
        for (int k = size-1; k >= index; k--)
            this.data[k+1] = this.data[k];
        data[index] = element;
        this.size++;
    }

    @Override
    public E remove(int index) throws IndexOutOfBoundsException {
        this.checkIndex(index, this.size);
        E temp = this.data[index];
        for (int k = index; k < size-1; k++)
            this.data[k] = data[k+1];
        this.data[size-1] = null;
        this.size--;
        return temp;
    }

    protected void checkIndex(int index, int bound) throws IndexOutOfBoundsException {
        if (index < 0 || index >= bound)
            throw new IndexOutOfBoundsException("Illegal index: " + index);
    }
    protected void resize(int capacity) {
        E[] temp = (E[]) new Object[capacity];
        for (int i = 0; i < this.size; i++)
            temp[i] = this.data[i];
        this.data = temp;
    }
    public Iterator<E> iterator() {
        return new ArrayIterator();
    }
}
