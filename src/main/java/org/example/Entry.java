package org.example;

public interface Entry<K, V> {
    /**
     *
     * @return the key stored in this entry
     */
    K getKey();

    /**
     *
     * @return the value stored in this entry
     */
    V getValue();
}
