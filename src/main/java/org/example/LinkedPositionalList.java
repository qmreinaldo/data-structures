package org.example;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedPositionalList<E> implements PositionalList<E> {
    private static class Node<E> implements Position<E> {
        private E element;
        private Node<E> prevNode;
        private Node<E> nextNode;
        public Node(E element, Node<E> prevNode, Node<E> nextNode) {
            this.element = element;
            this.prevNode = prevNode;
            this.nextNode = nextNode;
        }
        @Override
        public E getElement() throws IllegalStateException {
            if (this.nextNode == null) // Convention for defunct node
                throw new IllegalStateException("Position is no longer available");
            return this.element;
        }

        public Node<E> getPrevNode() {
            return this.prevNode;
        }

        public Node<E> getNextNode() {
            return this.nextNode;
        }
        public void setPrevNode(Node<E> prevNode) {
            this.prevNode = prevNode;
        }
        public void setNextNode(Node<E> nextNode) {
            this.nextNode = nextNode;
        }
        public void setElement(E element) {
            this.element = element;
        }
    }
    private class PositionIterator implements Iterator<Position<E>> {
        private Position<E> cursor = LinkedPositionalList.this.first(); // position of the next entity to report
        private Position<E> recent = null;                              // position of the last reported entity
        @Override
        public boolean hasNext() {
            return this.cursor != null;
        }

        @Override
        public Position<E> next() throws NoSuchElementException {
            if (this.cursor == null)
                throw new NoSuchElementException("nothing left");
            this.recent = this.cursor;
            this.cursor = LinkedPositionalList.this.after(this.cursor);
            return this.recent;
        }

        @Override
        public void remove() throws IllegalStateException {
            if (this.recent == null)
                throw new IllegalStateException("nothing to remove");
            LinkedPositionalList.this.remove(this.recent);
            this.recent = null;
        }
    }

    private class PositionIterable implements Iterable<Position<E>> {

        @Override
        public Iterator<Position<E>> iterator() {
            return new PositionIterator();
        }
    }

    private class ElementIterator implements Iterator<E> {
        Iterator<Position<E>> positionIterator = new PositionIterator();
        @Override
        public boolean hasNext() {
            return this.positionIterator.hasNext();
        }

        @Override
        public E next() {
            return this.positionIterator.next().getElement();
        }

        @Override
        public void remove() {
            this.positionIterator.remove();
        }
    }
    private Node<E> header;
    private Node<E> trailer;
    private int size = 0;

    public LinkedPositionalList() {
        this.header = new Node<>(null, null, null);
        this.trailer = new Node<>(null, header, null);
        this.header.setNextNode(this.trailer);
    }

    private Node<E> validate(Position<E> position) throws IllegalStateException {
        if (!(position instanceof Node))
            throw new IllegalStateException("Invalid position");
        Node<E> node = (Node<E>) position;
        if (node.getNextNode() == null)
            throw new IllegalArgumentException("Position is no longer in the list");
        return node;
    }

    private Position<E> toPosition(Node<E> node) {
        if (node == this.header || node == this.trailer)
            return null;
        return node;
    }
    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public Position first() {
        return this.toPosition(this.header.getNextNode());
    }

    @Override
    public Position last() {
        return this.toPosition(this.trailer.getPrevNode());
    }

    @Override
    public Position before(Position position) throws IllegalStateException {
        Node<E> node = this.validate(position);
        return this.toPosition(node.getPrevNode());
    }

    @Override
    public Position after(Position position) throws IllegalStateException {
        Node<E> node = this.validate(position);
        return this.toPosition(node.getNextNode());
    }

    private Position<E> addBetween(E element, Node<E> predecessor, Node<E> successor) {
        Node<E> newest = new Node<>(element, predecessor, successor);
        predecessor.setNextNode(newest);
        successor.setPrevNode(newest);
        size++;
        return newest;
    }

    @Override
    public Position<E> addFirst(E element) {
        return this.addBetween(element, this.header, this.header.getNextNode());
    }

    @Override
    public Position<E> addLast(E element) {
        return this.addBetween(element, this.trailer.getPrevNode(), this.trailer);
    }

    @Override
    public Position<E> addBefore(Position<E> position, E element) throws IllegalStateException {
        Node<E> node = this.validate(position);
        return this.addBetween(element, node.getPrevNode(), node);
    }

    @Override
    public Position<E> addAfter(Position<E> position, E element) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        return this.addBetween(element, node, node.getNextNode());
    }

    @Override
    public E set(Position<E> position, E element) throws IllegalStateException {
        Node<E> node = this.validate(position);
        E answer = node.getElement();
        node.setElement(element);
        return answer;
    }

    @Override
    public E remove(Position position) throws IllegalStateException {
        Node<E> node = this.validate(position);
        Node<E> predecessor = node.getPrevNode();
        Node<E> successor = node.getNextNode();
        predecessor.setNextNode(successor);
        successor.setPrevNode(predecessor);
        E answer = node.getElement();
        node.setElement(null);
        node.setNextNode(null);     // convention for defunct node
        node.setPrevNode(null);
        size--;
        return answer;
    }
    public Iterator<E> iterator() {
        return new ElementIterator();
    }
}
