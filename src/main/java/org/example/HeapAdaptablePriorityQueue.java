package org.example;

import java.util.Comparator;

public class HeapAdaptablePriorityQueue<K, V> extends HeapPriorityQueue<K, V>
                                    implements AdaptablePriorityQueue<K, V> {
    /* nested AdaptablePQEntry class */
    protected static class AdaptablePQEntry<K, V> extends PQEntry<K, V> {
        private int index;
        public AdaptablePQEntry(K key, V value, int index) {
            super(key, value);
            this.index = index;
        }
        public int getIndex() {
            return this.index;
        }
        public void setIndex(int index) {
            this.index = index;
        }
    }
    /* end of the nested class */

    public HeapAdaptablePriorityQueue() {
        super();
    }

    public HeapAdaptablePriorityQueue(Comparator<K> comparator) {
        super(comparator);
    }

    protected AdaptablePQEntry<K, V> validateEntry(Entry<K, V> entry) throws IllegalArgumentException {
        if (!(entry instanceof AdaptablePQEntry<K,V>))
            throw new IllegalArgumentException("Invalid entry");
        AdaptablePQEntry<K, V> locator = (AdaptablePQEntry<K, V>) entry;    // safe cast
        int i = locator.getIndex();
        if (i >= heap.size() || heap.get(i) != locator)
            throw new IllegalArgumentException("Invalid Entry");
        return locator;
    }

    protected void swap(int indexA, int indexB) {
        super.swap(indexA, indexB);
        ((AdaptablePQEntry<K, V>) heap.get(indexA)).setIndex(indexA);
        ((AdaptablePQEntry<K, V>) heap.get(indexB)).setIndex(indexB);
    }

    protected void bubble(int index) {
        if (index > 0 && compare(heap.get(index), heap.get(parentIndex(index))) < 0)
            upHeap(index);
        else
            downHeap(index);
    }

    public Entry<K, V> insert(K key, V value) throws IllegalArgumentException {
        checkKey(key);
        Entry<K, V> newestEntry = new AdaptablePQEntry<>(key, value, heap.size());
        heap.add(heap.size()-1, newestEntry);
        upHeap(heap.size()-1);
        return newestEntry;
    }


    @Override
    public void remove(Entry<K, V> entry) throws IllegalArgumentException {
        AdaptablePQEntry<K, V> locator = validateEntry(entry);
        int index = locator.getIndex();
        if (index == heap.size()-1)             // entry is at last position
            heap.remove(heap.size()-1);     // just remove it
        else {
            swap(index, heap.size()-1);     // swap entry to last pos
            heap.remove(heap.size()-1);     // remove it
            bubble(index);                          // place into the right pos
        }
    }

    @Override
    public void replaceKey(Entry<K, V> entry, K key) throws IllegalArgumentException {
        AdaptablePQEntry<K, V> locator = validateEntry(entry);
        checkKey(key);
        locator.setKey(key);
        bubble(locator.getIndex());     // place in the correct pos according to the key
    }

    @Override
    public void replaceValue(Entry<K, V> entry, V value) throws IllegalArgumentException {
        AdaptablePQEntry<K, V> locator = validateEntry(entry);
        locator.setValue(value);
    }
}
