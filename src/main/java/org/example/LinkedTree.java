package org.example;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LinkedTree<E> extends AbstractTree<E> {
    protected static class Node<E> implements Position<E> {
        private E element;
        private Node<E> parent;
        private PositionalList<Node<E>> children;
        public Node(E element, Node<E> parent, PositionalList<Node<E>> children) {
            this.element = element;
            this.parent = parent;
            this.children = children;
        }
        @Override
        public E getElement() throws IllegalStateException {
            return element;
        }
        public Node<E> getParent() {
            return this.parent;
        }
        public PositionalList<Node<E>> getChildren() {
            return this.children;
        }
        public void setElement(E element) {
            this.element = element;
        }
        public void setParent() {
            this.parent = parent;
        }

    }
    protected Node<E> createNode(E element, Node<E> parent, PositionalList<Node<E>> children) {
        return new Node<E>(element, parent, children);
    }

    protected Node<E> root = null;
    private int size = 0;
    public LinkedTree() {

    }

    protected Node<E> validate(Position<E> position) throws IllegalArgumentException {
        if (!(position instanceof Node))
            throw new IllegalArgumentException("Not valid position type.");
        Node<E> node = (Node<E>) position;
        if (node.getParent() == node)
            throw new IllegalArgumentException("position is no longer in the tree.");
        return node;
    }
    @Override
    public int size() {
        return this.size;
    }


    @Override
    public Position root() {
        return this.root;
    }

    @Override
    public Position parent(Position<E> position) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        return node.getParent();
    }

    @Override
    public Iterable<Position<E>> children(Position<E> position) throws IllegalArgumentException {
        List<Position<E>> snapshot = new ArrayList<>();
        Node<E> node = this.validate(position);
        for (Node<E> child : node.getChildren()) {
            snapshot.add(child);
        }
        return snapshot;
    }

    @Override
    public int numChildren(Position<E> position) throws IllegalArgumentException {
        Node<E> node = this.validate(position);
        return node.getChildren().size();
    }

    public Position<E> addRoot(E element) throws IllegalStateException{
        if (!this.isEmpty())
            throw new IllegalStateException("Tree is not empty");
        this.root = this.createNode(element, null, new LinkedPositionalList<>());
        this.size = 1;
        return this.root;
    }
    public Position<E> addFirstChild(Position<E> position, E element) {
        Node<E> parent = this.validate(position);
        Node<E> firstChild = this.createNode(element, parent, new LinkedPositionalList<>());
        parent.getChildren().addFirst(firstChild);
        this.size++;
        return firstChild;
    }
    public Position<E> addLastChild(Position<E> position, E element) {
        Node<E> parent = this.validate(position);
        Node<E> lastChild = this.createNode(element, parent, new LinkedPositionalList<>());
        parent.getChildren().addLast(lastChild);
        this.size++;
        return lastChild;
    }
    public E set(Position<E> position, E element) {
        Node<E> node = this.validate(position);
        E temp = node.element;
        node.setElement(element);
        return temp;
    }
}
