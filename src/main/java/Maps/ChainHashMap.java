package Maps;

import java.util.ArrayList;
import org.example.Entry;

public class ChainHashMap<K, V> extends AbstractHashMap<K, V>{
    private UnsortedTableMap<K, V>[] table;
    public ChainHashMap() {
        super();
    }
    public ChainHashMap(int capacity) {
        super(capacity);
    }
    public ChainHashMap(int capacity, int primeNum) {
        super(capacity, primeNum);
    }

    /**
     * Create an empty table having length equal to current capacity
     */
    protected void createTable() {
        table = (UnsortedTableMap<K, V>[]) new UnsortedTableMap[capacityTable];
    }

    /**
     * Returns values associated with key k in bucket with hash value h
     * @param hashValue
     * @param key
     * @return
     */
    protected V bucketGet(int hashValue, K key) {
        UnsortedTableMap<K, V> bucket = table[hashValue];
        if (bucket == null)
            return null;
        return bucket.get(key);
    }

    /**
     *
     * @param hashValue
     * @param key
     * @param value
     * @return
     */
    protected V bucketPut(int hashValue, K key, V value) {
       UnsortedTableMap<K, V> bucket = table[hashValue];
       if (bucket == null) {
           table[hashValue] = new UnsortedTableMap<>();
           bucket = table[hashValue];
       }
       int oldSize = bucket.size();
       V ans = bucket.put(key, value);
       nEntries += (bucket.size() - oldSize);   // size may have increased
        return ans;
    }

    protected V bucketRemove(int hashValue, K key) {
        UnsortedTableMap<K, V> bucket = table[hashValue];
        if (bucket == null)
            return null;
        int oldSize = bucket.size();
        V ans = bucket.remove(key);
        nEntries += (bucket.size()-oldSize);
        return ans;
    }

    public Iterable<Entry<K, V>> entrySet() {
        ArrayList<Entry<K, V>> buffer = new ArrayList<>();
        for (int h = 0; h < capacityTable; h++) {
            if (table[h] != null)
                for (Entry<K, V> entry: table[h].entrySet())
                    buffer.add(entry);
        }
        return buffer;
    }
}
