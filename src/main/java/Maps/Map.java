package Maps;

import org.example.Entry;

public interface Map<K, V> {
    int size();
    boolean isEmpty();

    /**
     * Returns the value v associated with key k, if such an entry exists.
     * Otherwise, returns null
     * @param key
     * @return value v or null
     */
    V get(K key);

    /**
     * If the map does not have an entry with key k, adds entry (k, v) and returns null.
     * Otherwise, replaces with value the existing value of the entry with the given key and returns the old value
     * @param key
     * @param value
     * @return
     */
    V put(K key, V value);

    /**
     * Removes the entry associated to the given key, if such a entry exists.
     * Otherwise, returns null
     * @param key
     * @return
     */
    V remove(K key);

    /**
     * Returns an iterable collection of all keys
     * @return
     */
    Iterable<K> keySet();

    /**
     * Returns an iterable collection of all values. It can be repetitions
     * @return
     */
    Iterable<V> values();

    /**
     * Returns an iterable collection of all pairs (k, v)
     * @return
     */
    Iterable<Entry<K, V>> entrySet();
}
