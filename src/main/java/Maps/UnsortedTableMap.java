package Maps;

import org.example.ArrayList;
import org.example.Entry;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class UnsortedTableMap<K, V> extends AbstractMap<K, V> {
    private ArrayList<MapEntry<K, V>> table = new ArrayList<>();

    public UnsortedTableMap() {

    }

    // private utility
    private int findIndex(K key) {
        int n = table.size();
        for (int i = 0; i < n; i++)
            if (table.get(i).getKey().equals(key))
                return i;
        return -1;  // key was not found
    }
    @Override
    public int size() {
        return table.size();
    }

    /**
     * Returns the value v associated with key k, if such an entry exists.
     * Otherwise, returns null
     *
     * @param key
     * @return value v or null
     */
    @Override
    public V get(K key) {
        int index = findIndex(key);
        if (index == -1)
            return null;
        return table.get(index).getValue();
    }

    /**
     * If the map does not have an entry with key k, adds entry (k, v) and returns null.
     * Otherwise, replaces with value the existing value of the entry with the given key and returns the old value
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public V put(K key, V value) {
        int index = findIndex(key);
        if (index == -1){
            table.add(table.size()-1, new MapEntry<>(key, value));
            return null;
        }
        return table.get(index).getValue();
    }

    /**
     * Removes the entry associated to the given key, if such a entry exists.
     * Otherwise, returns null
     *
     * @param key
     * @return
     */
    @Override
    public V remove(K key) {
        int index = findIndex(key);
        int n = this.size();
        if (index == -1)
            return null;
        V ans = table.get(index).getValue();
        if (index != n-1)
            table.set(index, table.get(n-1));   // relocate the last entry to fulfill the hole
        table.remove(n-1);
        return ans;
    }

    // Support for public entrySet method
    private class EntryIterator implements Iterator<Entry<K, V>> {
        private int j = 0;
        public boolean hasNext() {
            return j < table.size();
        }
        public Entry<K, V> next() {
            if (j == table.size())
                throw new NoSuchElementException();
            return table.get(j++);
        }
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private class EntryIterable implements Iterable<Entry<K, V>> {
        public Iterator<Entry<K, V>> iterator() {
            return new EntryIterator();
        }
    }

    /**
     * Returns an iterable collection of all pairs (k, v)
     *
     * @return
     */
    @Override
    public Iterable<Entry<K, V>> entrySet() {
        ;return new EntryIterable();
    }
}
