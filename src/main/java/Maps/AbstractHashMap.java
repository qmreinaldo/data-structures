package Maps;

import org.example.Entry;

import java.util.ArrayList;
import java.util.Random;

public abstract class AbstractHashMap<K, V> extends AbstractMap<K, V> {
    protected int nEntries = 0;
    protected int capacityTable;
    private int primeNum;
    private long scale, shift;
    private final static int DEFAULT_CAPACITY = 17;
    private final static int DEFAULT_PRIME = 109345121;
    public AbstractHashMap(int capacityTable, int primeNum) {
        this.primeNum = primeNum;
        this.capacityTable = capacityTable;
        Random random = new Random();
        scale = random.nextInt(primeNum-1)+1;
        shift = random.nextInt(primeNum);
        createTable();
    }

    public AbstractHashMap(int capacityTable) {
        this(capacityTable, DEFAULT_PRIME);
    }
    public AbstractHashMap() {
        this(DEFAULT_CAPACITY);
    }

    // public methods
    public int size() {
        return this.nEntries;
    }

    public V get(K key) {
        return bucketGet(hashValue(key), key);
    }
    public V remove(K key) {
        return bucketRemove(hashValue(key), key);
    }
    public V put(K key, V value) {
        V ans = bucketPut(hashValue(key), key, value);
        if (nEntries > capacityTable/2)     // load factor should be < 0.5
            this.resize(2*capacityTable-1);
        return ans;
    }
    // private utilities
    private int hashValue(K key) {
        return (int) (Math.abs(key.hashCode()*scale + shift) % capacityTable);
    }
    private void resize(int newCapacity) {
        ArrayList<Entry<K, V>> buffer = new ArrayList<>(newCapacity);
        for (Entry<K, V> entry : this.entrySet())
            buffer.add(entry);
        this.capacityTable = newCapacity;
        createTable();
        this.nEntries = 0;  // it will be recomputed while reinserting entries
        for (Entry<K, V> entry : buffer)
            this.put(entry.getKey(), entry.getValue());
    }

    // protected abstract methods to be implemented by concrete subclasses
    protected abstract void createTable();
    protected abstract V bucketGet(int hashValue, K key);
    protected abstract V bucketPut(int hashValue, K key, V value);
    protected abstract V bucketRemove(int hashValue, K key);
}
