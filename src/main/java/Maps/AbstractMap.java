package Maps;

import org.example.Entry;


import java.util.Iterator;

public abstract class AbstractMap<K, V> implements Map<K, V> {
    // Nested MapEntry class
    protected static class MapEntry<K, V> implements Entry<K, V> {
        private K key;
        private V value;
        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        /**
         * @return the key stored in this entry
         */
        @Override
        public K getKey() {
            return this.key;
        }

        /**
         * @return the value stored in this entry
         */
        @Override
        public V getValue() {
            return this.value;
        }
        // utilities not exposed as part of the Entry interface
        protected void setKey(K key) {
            this.key = key;
        }
        protected V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    } // end of nested MepEntry class


    // Support for public keySet() method
    private class KeyIterator implements Iterator<K> {
        private final Iterator<Entry<K, V>> entries = entrySet().iterator();  // reuse entrySet()
        public boolean hasNext() {
            return entries.hasNext();
        }
        public K next() {
            return entries.next().getKey(); // return key
        }
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private class KeyIterable implements Iterable<K> {
        public Iterator<K> iterator(){
            return new KeyIterator();
        }
    }

    // Support for public values() method
    private class ValueIterator implements Iterator<V> {
        private final Iterator<Entry<K, V>> entries = entrySet().iterator();  // reuse entrySet()
        public boolean hasNext() {
            return entries.hasNext();
        }
        public V next() {
            return entries.next().getValue();   // return value
        }
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    private class ValueIterable implements Iterable<V> {
        public Iterator<V> iterator() {
            return new ValueIterator();
        }
    }



    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns an iterable collection of all keys
     * @return
     */
    @Override
    public Iterable<K> keySet() {
        return new KeyIterable();
    }

    @Override
    public Iterable<V> values() {
        return new ValueIterable();
    }
}
