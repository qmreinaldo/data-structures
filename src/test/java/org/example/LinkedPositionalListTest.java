package org.example;

import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class LinkedPositionalListTest {
    @Test
    public void shouldRemoveFirstElementUsingIterator() {
        LinkedPositionalList<Integer> positionalList = new LinkedPositionalList<>();
        positionalList.addLast(1);
        positionalList.addLast(2);
        positionalList.addLast(3);
        Iterator<Integer> iterator = positionalList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == 1)
                iterator.remove();
        }
        assertEquals(2, positionalList.size());
    }

    @Test
    public void shouldRemoveAllElementsUsingIterator() {
        LinkedPositionalList<Integer> positionalList = new LinkedPositionalList<>();
        positionalList.addLast(1);
        positionalList.addLast(2);
        positionalList.addLast(3);
        Iterator<Integer> iterator = positionalList.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
        assertEquals(0, positionalList.size());
    }
}