package org.example;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LinkedTreeTest {
    @Test
    public void shouldAddARootToAnEmptyLinkedTree() {
        LinkedTree<Integer> linkedTree = new LinkedTree<>();
        linkedTree.addRoot(1);
        assertEquals(1, linkedTree.root().getElement());
    }
    @Test
    public void shouldAddAFirstAChild() {
        LinkedTree<Integer> linkedTree = new LinkedTree<>();
        Position<Integer> root = linkedTree.addRoot(1);
        linkedTree.addFirstChild(root, 2);

        List<Integer> result = new ArrayList<>();

        for (Position<Integer> child : linkedTree.children(root)) {
            result.add(child.getElement());
        }

        List<Integer> expected = new ArrayList<>();
        expected.add(2);

        assertEquals(expected, result);
    }
    @Test
    public void shouldAddAFirstAndLastChild() {
        LinkedTree<Integer> linkedTree = new LinkedTree<>();
        Position<Integer> root = linkedTree.addRoot(1);
        linkedTree.addFirstChild(root, 2);
        linkedTree.addLastChild(root, 3);

        List<Integer> result = new ArrayList<>();

        for (Position<Integer> child : linkedTree.children(root)) {
            result.add(child.getElement());
        }

        List<Integer> expected = new ArrayList<>();
        expected.add(2);
        expected.add(3);

        assertEquals(expected, result);
    }
    @Test
    public void shouldSetANewValueForAPosition() {
        LinkedTree<Integer> linkedTree = new LinkedTree<>();
        Position<Integer> root = linkedTree.addRoot(1);
        Position<Integer> child = linkedTree.addFirstChild(root, 2);
        linkedTree.set(child, 4);

        assertEquals(4, child.getElement());
    }
}