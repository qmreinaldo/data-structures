package org.example;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class LinkedBinaryTreeTest<E> {
    @Test
    public void shouldHaveZeroSize() {
        LinkedBinaryTree<E> linkedBinaryTree = new LinkedBinaryTree<>();
        assertEquals(0, linkedBinaryTree.size());
    }
    @Test
    public void shouldHaveSizeOne() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        linkedBinaryTree.addRoot(1);
        assertEquals(1, linkedBinaryTree.size());
    }
    @Test
    public void shouldAddARootToAnEmptyTree() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        linkedBinaryTree.addRoot(1);
        assertEquals(1, linkedBinaryTree.root().getElement());
    }
    @Test
    public void shouldNotAddRootToAnNonEmptyTree() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        linkedBinaryTree.addRoot(1);
        Exception exception = assertThrows(IllegalStateException.class, () ->
                linkedBinaryTree.addRoot(2));
        String expectedMessage = "Tree is not empty";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    public void shouldAddALeftChild() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);
        linkedBinaryTree.addLeft(root, 2);

        assertEquals(2, linkedBinaryTree.left(root).getElement());
    }
    @Test
    public void shouldAddRightChild() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);
        linkedBinaryTree.addRight(root, 3);

        assertEquals(3, linkedBinaryTree.right(root).getElement());
    }
    @Test
    public void shouldChangeTheElementStoredAtNode() {
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);
        linkedBinaryTree.set(root, 10);

        assertEquals(10, linkedBinaryTree.root().getElement());
    }
    @Test
    public void shouldAttachASubtreeAtLeft() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);

        LinkedBinaryTree<Integer> leftSubtree = new LinkedBinaryTree<>();
        leftSubtree.addRoot(10);

        LinkedBinaryTree rightSubtree = new LinkedBinaryTree();

        linkedBinaryTree.attach(root, leftSubtree, rightSubtree);

        assertEquals(10, linkedBinaryTree.left(root).getElement());
    }
    @Test
    public void shouldAttachASubtreeAtRight() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);

        LinkedBinaryTree<Integer> rightSubtree = new LinkedBinaryTree<>();
        rightSubtree.addRoot(10);

        LinkedBinaryTree leftSubtree = new LinkedBinaryTree();

        linkedBinaryTree.attach(root, leftSubtree, rightSubtree);

        assertEquals(10, linkedBinaryTree.right(root).getElement());
    }
    @Test
    public void shouldRemoveTheRoot() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);

        linkedBinaryTree.remove(root);

        assertNull(linkedBinaryTree.root());
    }

    @Test
    public void shouldRemoveALeftChildWithNoGrandChildren() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);
        Position<Integer> leftChild = linkedBinaryTree.addLeft(root, 2);
        linkedBinaryTree.remove(leftChild);

        assertNull(linkedBinaryTree.left(root));
    }
    @Test
    public void shouldRemoveArightChildWithNoGrandChildren() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);
        Position<Integer> rightChild = linkedBinaryTree.addRight(root, 3);

        linkedBinaryTree.remove(rightChild);

        assertNull(linkedBinaryTree.right(root));
    }
    @Test
    public void shouldRemoveLeftChildReplacingWithGrandchild() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);
        Position<Integer> leftChild = linkedBinaryTree.addLeft(root, 2);
        linkedBinaryTree.addRight(leftChild, 5);

        linkedBinaryTree.remove(leftChild);

        assertEquals(5, linkedBinaryTree.left(root).getElement());
    }
    @Test
    public void shouldRemoveRightChildReplacingWithGrandchild() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> root = linkedBinaryTree.addRoot(1);
        Position<Integer> rightChild = linkedBinaryTree.addRight(root, 2);
        linkedBinaryTree.addLeft(rightChild, 6);

        linkedBinaryTree.remove(rightChild);

        assertEquals(6, linkedBinaryTree.right(root).getElement());
    }
}