package org.example;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AbstractTreeTest {

    @Test
    public void shouldReportInPreorderBinaryTree() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> first = linkedBinaryTree.addRoot(1);
        Position<Integer> second = linkedBinaryTree.addLeft(first, 2);
        linkedBinaryTree.addLeft(second, 3);
        linkedBinaryTree.addRight(second, 4);
        linkedBinaryTree.addRight(first, 5);

        Iterable<Position<Integer>> positionsList =  linkedBinaryTree.positions();
        List<Integer> result = new ArrayList<>();
        for (Position<Integer> pos: positionsList)
            result.add(pos.getElement());
        Integer[] elements = new Integer[] {1, 2, 3, 4, 5};
        List<Integer> expected = new ArrayList<>(Arrays.asList(elements));

        assertTrue(expected.equals(result));
    }

    @Test
    public void shouldReportInBreadthFirstForLinkedBinaryTree() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Position<Integer> first = linkedBinaryTree.addRoot(1);
        Position<Integer> second = linkedBinaryTree.addLeft(first, 2);
        linkedBinaryTree.addLeft(second, 4);
        linkedBinaryTree.addRight(second, 5);
        linkedBinaryTree.addRight(first, 3);

        Iterable<Position<Integer>> positionList = linkedBinaryTree.breadthFirst();
        List<Integer> result = new ArrayList<>();
        for (Position<Integer> pos: positionList)
            result.add(pos.getElement());
        Integer[] elements = new Integer[] {1, 2, 3, 4, 5};
        List<Integer> expected = new ArrayList<>(Arrays.asList(elements));

        assertTrue(expected.equals(result));
    }

}