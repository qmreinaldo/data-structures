package org.example;

import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class ArrayListTest {

    @Test
    public void shouldRemoveFirstElementUsingIterator() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(0, 1);
        arrayList.add(1, 2);
        arrayList.add(2, 3);
        Iterator<Integer> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == 1)
                iterator.remove();
        }
        assertEquals(2, arrayList.size());
    }
    @Test
    public void shouldRemoveAllElementsUsingIterator() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(0, 1);
        arrayList.add(1, 2);
        arrayList.add(2, 3);
        Iterator<Integer> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
        assertEquals(0, arrayList.size());
    }
}